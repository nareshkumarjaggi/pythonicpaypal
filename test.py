#! /usr/bin/python3
# -*- coding: utf-8 -*-

from paypal import ConnectPaypal, Tracker, Permission
import requests

##########################################
# Global variable defined below need to
# be updated before testing it on sandbox
# using your own account.
#######################################

demo_client_id = "Acl1YE0C0ymzjOjM_Zdjy0XYwdiJ070SGFIC0Uk7v3929KVGvm_GoNnH8KzcYKpD66fzSmGugqSHwYAC"
demo_client_secret = "EEga4WledcS0rgXfGm2LWMG39XOZXnmCwEayIWmomqd-ycDGnU9mOp_bpgXdpjvz_MYbNI7Xw0GxRG38"
demo_transaction_id = "000000"
demo_tracking_number = "XYZ0000"
demo_update_tracker_data = {
    "transaction_id": "8MC585209K746392H",
    "tracking_number": "443844607820",
    "status": "SHIPPED",
    "carrier": "FEDEX"
}
demo_add_tracker_data =  {
    "trackers": [{
        "transaction_id": "8MC585209K746392H",
        "status": "SHIPPED",
        "carrier": "FEDEX"
    }]
}

demo_permision_user_id = "nkumarjaggi-facilitator_api1.gmail.com"
demo_permission_password = "4F2HRNRMSE2HJTDS"
demo_permission_signature = "AblxiQj3i37NKgluHKB9h-hx8zrkAj5K4IgSnzRqEOVp3cBjYXojkaZw"
demo_permission_data = {
    "scope": "EXPRESS_CHECKOUT",
    "callback": "https://example.com/success.html",
    "requestEnvelope": {"errorLanguage": "en_US"
    }
    }


def test_paypal_connection():
    """
    mthod used to test the paypal connection
    """
    cp = ConnectPaypal(demo_client_id,
                       demo_client_secret)
    cp.get_authorization_headers
    cp.check_connection
    cp.get_application_id
    return cp

def test_tracker(paypal_connection_instance):
    """
    Method used to test the paypal
    Tracker class
    """
    tracker = Tracker(paypal_connection_instance)
    try:
        tracker.get_tracking_information(demo_transaction_id, demo_tracking_number)
    except requests.exceptions.HTTPError as e:
        print("Exceptin raised: \n %s" % e)
    try:
        tracker.update_tracking_information(demo_update_tracker_data, demo_transaction_id, demo_tracking_number)
    except requests.exceptions.HTTPError as e:
        print("Exceptin raised: \n %s" % e)
    try:
        tracker.add_tracker(demo_add_tracker_data)
    except requests.exceptions.HTTPError as e:
        print("Exceptin raised: \n %s" % e)


def test_permission(paypal_connection_instance):
    """
    Method used to test the pemisson_class
    used to facilate the paypal permission
    service
    """
    permisson_instance = Permission(paypal_connection_instance)
    permisson_instance.request_permission(demo_permision_user_id, demo_permission_password, demo_permission_signature, demo_permission_data)

if __name__ == "__main__":
    cp = test_paypal_connection()
    test_tracker(cp)
    test_permission(cp)
