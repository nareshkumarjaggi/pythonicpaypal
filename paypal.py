#!/usr/bin/pythoh3
#-*- coding:utf-8 -*-

import requests
import datetime as dt
import json


class ConnectPaypal:
    """
    class used to connect to 
    PaypalREST API and get the
    Authentication tocken back
    in response
    """
    def __init__(self, client_id, secret, *args, **kwargs):
        self.client_id = client_id
        self.secret = secret
        
        # Lifecycle Methods
        self.get_access_token()


    def get_access_token(self):
        """
        Method used to call paypal
        RESTAPI and get access_token
        using clinet_id and secret
        of the instance.
        """
        print("Calling Paypal for Access Token")
        headers = {
                'Accept': 'application/json',
                'Accept-Language': 'en_US',
        }
        data = {'grant_type': 'client_credentials'}
        response = requests.post('https://api.sandbox.paypal.com/v1/oauth2/token',
                                 headers=headers,
                                 data=data, auth=(self.client_id, self.secret))
        print("Response Text : \n %s \n " % response.text)
        response.raise_for_status()
        self.access_token_data = response.json()
        self.expire_timestamp = dt.datetime.now() + dt.timedelta(seconds=self.access_token_data['expires_in'])
        print("Response Status code : %s" % response.status_code)

    def get_authorization_headers(self):
        """
        Method used to create the Bearer
        Authentication headers on the go
        with all other necessery headers
        required for the API Call
        """
        print("creating authentication headers")
        return {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer %s' % (self.access_token_data['access_token']),
        }

    @property
    def check_connection(self):
        """
        method used to check if the token
        has expired or not.
        """
        if dt.datetime.now() < self.expire_timestamp:
            return True
        else:
            return False

    @property
    def get_application_id(self):
        return self.access_token_data['app_id']


class Tracker:
    """
    Class Used to work with Payal
    Tracking API
    """

    def __init__(self, paypal_connection, *args, **kwargs):
        self.paypal_connection = paypal_connection

    def __check_paypal_connection__(self):
        """
        Method used to check if paypal
        Connection instance is valid
        as Token expires after ever 3600
        second by default, If Token is expired
        a new valid instance is genrated agin.
        """
        if self.paypal_connection.check_connection is True:
            pass
        else:
            self.paypal_connection = ConnectPaypal(self.paypal_connection.client_id, self.paypal_connection.secret)

    def get_tracking_information(self, transaction_id, tracking_number, *args, **kwargs):
        """
        Method used to get the tracking information using
        :transaction_id
        :tracking_number
        """
        self.__check_paypal_connection__()
        response = requests.get('https://api.sandbox.paypal.com/v1/shipping/trackers/%s-%s' % (transaction_id, tracking_number),
                                headers=self.paypal_connection.get_authorization_headers())
        print("Tracking informations : \n %s \n" % (response.text))
        response.raise_for_status()
        print("Response status code: %s" % (response.status_code))
        return response.json()

    def update_tracking_information(self, data, transaction_id, tracking_number, *args, **kwargs):
        """
        Method used to udpate the tracking information using
        :data (dict)
        :transaction_id
        :tracking_number
        """
        self.__check_paypal_connection__()
        response = requests.put('https://api.sandbox.paypal.com/v1/shipping/trackers/%s-%s' % (transaction_id, tracking_number),
                                headers=self.paypal_connection.get_authorization_headers(),
                                data=json.dumps(data))
        print("Update Transaction response:\n %s \n" % response.text)
        response.raise_for_status()
        print("Response status code: %s" % (response.status_code))
        return response.json()

    def add_tracker(self, data, *args, **kwargs):
        """
        Method used to add the trackers for transaction
        in batch of transactions, using
        :data (dict)
        """
        self.__check_paypal_connection__()
        response = requests.post('https://api.sandbox.paypal.com/v1/shipping/trackers-batch',
                                 headers=self.paypal_connection.get_authorization_headers(),
                                 data=json.dumps(data))
        print("Add Tracker API call Response %s" % response.text)
        response.raise_for_status()
        return response.json()

class Permission:
    """
    Class used to implement paypal
    Permission service
    """

    def __init__(self, paypal_connection_instance):
        self.cp = paypal_connection_instance

    def __check_paypal_connection__(self):
        """
        Method used to check if paypal
        Connection instance is valid
        as Token expires after ever 3600
        second by default, If Token is expired
        a new valid instance is genrated agin.
        """
        if self.cp.check_connection is True:
            pass
        else:
            self.cp = ConnectPaypal(self.cp.client_id, self.cp.secret)


    def request_permission(self, security_user_id, security_password, security_signatur_data, data, *args, **kwargs):
        """
        Method used to request permission form paypal
        client
        """
        self.__check_paypal_connection__()
        headers = {"X-PAYPAL-SECURITY-USERID": security_user_id,
                   "X-PAYPAL-SECURITY-PASSWORD": security_password,
                   "X-PAYPAL-SECURITY-SIGNATURE": security_signatur_data,
                    "X-PAYPAL-APPLICATION-ID": self.cp.get_application_id,
                    "X-PAYPAL-REQUEST-DATA-FORMAT" : "JSON",
                   "X-PAYPAL-RESPONSE-DATA-FORMAT" : "JSON"}
        
        response = requests.post('https://svcs.sandbox.paypal.com/Permissions/RequestPermissions',
                                 headers=headers,
                                 data=json.dumps(data),
                                 verify=False)
        print("permisson_request reponse :\n %s \n" % (response.text))
        response.raise_for_status()
        return response.json()
