# PythonicPaypal
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This module is capable of accessing the Paypal's REST API using python request 
module, PyhtonicPaypal module contains 3 main classes, which are kept in paypal.py file in the root of the module
  - ConnectPaypal
  - Tracker
  - Permission

# ConnectPaypal Class

  - This class except for two positional arguments <clinet_id> and  <secret>, and get authentication token using these two credential, which will be used in further API calls to PayPal HTTP API. An instance of this class will be used by other classes also like Tracker and permission class, To keep a live connection with PayPal API
 
# Tracker class
  - This class is used to add trackers, update tracking information or create tracker in bulk for created transactions. It accepts only one pythonic positional argument which is a paypal_connection_instance

# Permission class
  - This is used to initiate the third party checkout permissions (ie: express_checkouts). It will accept only one Positional argument which is a paypal_connection instance
  
> For Checking the functionality of the code
> we need to setup virtual environment for better testing

### Setting up local DEV environment for testing the codebase
### creating a virtual environment

PythonicPaypal requires [python3](https://www.python.org/download/releases/3.0/) 

```sh
$ virtualenv -p python3 pythonic_paypal_env
$ cd pythonic_paypal_env

# fork your repository or use one given below to clone form

$ git clone https://nareshkumarjaggi@bitbucket.org/nareshkumarjaggi/pythonicpaypal.git
$ cd pythonicpaypal

```
### Installing requirements.

```sh
$ source ../bin/activate
$ pip install -r requirements.txt
```

```sh
# opening Ipython shell installed by requirements file
$ ipython
```

>Inside Ipython shell perform following commands or alternatively update the global
>variable section (separated by comments) in test.py with your API credentials as it is filled with dummy data, and run the test.py file as a standalone python module
> to view API response on the shell STDOUT, if you didn't get what positional argument in the below-given code represents, Use the test.py file for API Call reference.

```py
from paypal import (ConnectPaypal, Tracker, Permission)

# we are providing client ID and the secret to ConnectPaypal instance
# in order to generate the Bearer Authorization token which will be used
# for further API Calls.

paypal_connection_instance = ConnectPaypal(<your_clined_id>, <your_secret>)

# to know about paypal_app_id run following command
paypal_connection_instance.get_application_id

# To know about the authorization Headers for further API calls
paypal_connection_instance.get_authorization_headers()

# To get tracking information we need to set up the Tracker class
# instance. Which need the ConnectPaypal instance to keep
# connection live the Paypal's web_api

tracker = Tracker(paypal_connection_instance)

# getting tracking information using transaction_id and tracking number
tracking_information = tracker.get_tracking_information(demo_transaction_id, demo_tracking_number)

# To update already existing tracking information use following function
tracker.update_tracking_information(<updated_tracker_data_in_dict_form>, <transaction_id>, <tracking_number>)

# To add trackers in buld for transactions, Use following function
tracker.add_tracker(<bulk_add_tracker_data>)

# To add third party permissions use Permission Class

permission_insatance = Permission(paypal_connection_instance)

# for requesting permissions use folowing function
permission_instance.request_permission(<security_user_id>, <security_password>, <security_signatur_data>, <permission_data>)

# to know about permission_data format refer the test.py file
```

